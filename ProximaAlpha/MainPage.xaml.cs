﻿#define DEBUG_AGENT

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Newtonsoft.Json;
using Microsoft.Phone.Scheduler;

using System.IO;
using System.Threading;


namespace ProximaAlpha
{
    public partial class MainPage : PhoneApplicationPage
    {


        PeriodicTask periodicTask;
        ResourceIntensiveTask resourceIntensiveTask;

        string periodicTaskName = "PeriodicAgent";
        string resourceIntensiveTaskName = "ResourceIntensiveAgent";
        public bool agentsAreEnabled = true;
        bool ignoreCheckBoxEvents = false;




        String action = "SignIn";
        String getdata;
        public MainPage()
        {
            InitializeComponent();
        }

        private void SingInButton_Click(object sender, RoutedEventArgs e)
        {
            string signedInEmail = EmailIDBoxMain.Text;
            string password = PasswordBoxMain.Password;
            var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action=" + action + "&signedInEmail=" + signedInEmail + "&password=" + password) as HttpWebRequest;
            request.Accept = "application/json;odata=verbose";
            request.BeginGetResponse(new AsyncCallback(GotResponse), request);


        }

        private void Authenticate()
        {

            if (getdata == "0")
            {
                MessageBox.Show("Signing in failed. Please double-check your E-mail ID and re-enter your password.");
                EmailIDBoxMain.Text = "";
                PasswordBoxMain.Password = "";
            }
            else
            {
                string password = PasswordBoxMain.Password;
                NavigationService.Navigate(new Uri("/PeoplePage.xaml?&signedInEmail=" + EmailIDBoxMain.Text + "&serializedUserInfo=" + getdata + "&password=" + password, UriKind.Relative));
            }
        }


        private void GotResponse(IAsyncResult asynchronousResult)
        {
            try
            {
                string data;
                // State of request is asynchronous 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)asynchronousResult.AsyncState; ;
                using (HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.EndGetResponse(asynchronousResult))
                {
                    // Read the response into a Stream object. 
                    System.IO.Stream responseStream = response.GetResponseStream();
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        data = reader.ReadToEnd();
                    }
                    responseStream.Close();
                }
                // Callback occurs on a background thread, so use Dispatcher to marshal back to the UI thread 
                this.Dispatcher.BeginInvoke(() =>
                {
                    getdata = data;
                    Authenticate();
                    MessageBox.Show("Received payload of " + data.Length + " characters \nRecieved:" + data);
                });
            }
            catch (Exception e)
            {
                var we = e.InnerException as WebException;
                if (we != null)
                {
                    var resp = we.Response as HttpWebResponse;
                    var code = resp.StatusCode;
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show("RespCallback Exception raised! Message:" + we.Message + "HTTP Status: " + we.Status);
                    });
                }
                else
                    throw;
            }
        }


        private void StartPeriodicAgent()
        {
            // Variable for tracking enabled status of background agents for this app.
            agentsAreEnabled = true;

            // Obtain a reference to the period task, if one exists
            periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

            // If the task already exists and background agents are enabled for the
            // application, you must remove the task and then add it again to update 
            // the schedule
            if (periodicTask != null)
            {
                RemoveAgent(periodicTaskName);
            }

            periodicTask = new PeriodicTask(periodicTaskName);

            // The description is required for periodic agents. This is the string that the user
            // will see in the background services Settings page on the device.
            periodicTask.Description = "Something proper";

            // Place the call to Add in a try block in case the user has disabled agents.
            try
            {
                ScheduledActionService.Add(periodicTask);
                PeriodicStackPanel.DataContext = periodicTask;

                // If debugging is enabled, use LaunchForTest to launch the agent in one minute.
#if(DEBUG_AGENT)
                ScheduledActionService.LaunchForTest(periodicTaskName, TimeSpan.FromSeconds(60));
#endif
            }
            catch (InvalidOperationException exception)
            {
                if (exception.Message.Contains("BNS Error: The action is disabled"))
                {
                    MessageBox.Show("Background agents for this application have been disabled by the user.");
                    agentsAreEnabled = false;
                    PeriodicCheckBox.IsChecked = false;
                }

                if (exception.Message.Contains("BNS Error: The maximum number of ScheduledActions of this type have already been added."))
                {
                    // No user action required. The system prompts the user when the hard limit of periodic tasks has been reached.

                }
                PeriodicCheckBox.IsChecked = false;
            }
            catch (SchedulerServiceException)
            {
                // No user action required.
                PeriodicCheckBox.IsChecked = false;
            }

        }

        private void PeriodicCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (ignoreCheckBoxEvents)
                return;
           
            
            StartPeriodicAgent();
            MessageBox.Show("over " + periodicTask.IsScheduled);
           // MessageBox.Show("below " + periodicTask.BeginTime);
            MessageBox.Show("below " + periodicTask.Description);
        }
        private void PeriodicCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (ignoreCheckBoxEvents)
                return;
            RemoveAgent(periodicTaskName);
        }
        private void ResourceIntensiveCheckBox_Checked(object sender, RoutedEventArgs e)
        {
           /* if (ignoreCheckBoxEvents)
                return;
            StartResourceIntensiveAgent();*/
        }
        private void ResourceIntensiveCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
          /*  if (ignoreCheckBoxEvents)
                return;
            RemoveAgent(resourceIntensiveTaskName);*/
        }

        private void RemoveAgent(string name)
        {
            try
            {
                ScheduledActionService.Remove(name);
            }
            catch (Exception)
            {
            }
        }


        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            ignoreCheckBoxEvents = true;

            periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;
            

            if (periodicTask != null)
            {
                PeriodicStackPanel.DataContext = periodicTask;
            }

            ignoreCheckBoxEvents = false;
        }

    }
}
