﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using System.Device.Location;
using Microsoft.Phone.Controls.Maps;
using System.Windows.Media;

namespace ProximaAlpha
{
    public partial class LocationPage : PhoneApplicationPage
    {
        UserInfo current;
        String serializedUserInfo,action,getdata;
        String signedInUID,signedInName,selectedIndex, selectedName;
        private GeoCoordinateWatcher gps;
        int i;
        double latitude;
        double longitude;

        public LocationPage()
        {
            InitializeComponent();
           // GPSClass locator = new GPSClass();
            //locator.getGPScoordinates();
            
        }


        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("serializedUserInfo", out serializedUserInfo) && NavigationContext.QueryString.TryGetValue("selectedIndex", out selectedIndex))
            {
                current = (UserInfo)JsonConvert.DeserializeObject<UserInfo>(serializedUserInfo);
                signedInUID = current.fromUID;
                selectedName = current.contact[Convert.ToInt32(selectedIndex)].name;
                locationPageTitleBlock.Text = selectedName;
                
                
                MessageBox.Show("Selected name:"+selectedName);
                MapLayer m_PushpinLayer = new MapLayer();
                map.Children.Add(m_PushpinLayer);
                String markLocation = current.contact[Convert.ToInt32(selectedIndex)].lastLocated;
                i = 0;
                if (!markLocation.Equals("null"))
                {

                    i =markLocation.IndexOf(",");
                    
                    Double latitude = Convert.ToDouble(markLocation.Substring(0, i));
                    Double longitude = Convert.ToDouble(markLocation.Substring(i + 1, markLocation.Length - i - 1));
                    GeoCoordinate coordinates = new GeoCoordinate(latitude, longitude);
                    map.SetView(coordinates, 10);

                    String locatedTime = current.contact[Convert.ToInt32(selectedIndex)].locatedTime;
                    LocationTimeBlock.Text = locatedTime;
                    Pushpin pushpin1 = new Pushpin();
                    pushpin1.Background = new SolidColorBrush(Colors.LightGray);
                    pushpin1.Location = coordinates;
                    m_PushpinLayer.AddChild(pushpin1, pushpin1.Location);

                    getPresentLocation();
                }
                // Reasonable stab at center of Seattle metro area
                else MessageBox.Show("He's dead");
            }
        }


        protected void getPresentLocation()
        {
            action = "FETCHLOCATION";
            var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action=" + action + "&signedInUID=" + signedInUID + "&requestedUID=" + current.contact[Convert.ToInt32(selectedIndex)].uid) as HttpWebRequest;
            request.Accept = "application/json;odata=verbose";
            request.BeginGetResponse(new AsyncCallback(GotResponse), request);
        
        }


        private void GotResponse(IAsyncResult asynchronousResult)
        {
            try
            {
                string data;
                // State of request is asynchronous 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)asynchronousResult.AsyncState; ;
                using (HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.EndGetResponse(asynchronousResult))
                {
                    // Read the response into a Stream object. 
                    System.IO.Stream responseStream = response.GetResponseStream();
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        data = reader.ReadToEnd();
                    }
                    responseStream.Close();
                }
                // Callback occurs on a background thread, so use Dispatcher to marshal back to the UI thread 
                this.Dispatcher.BeginInvoke(() =>
                {
                    getdata = data;
                    //MessageBox.Show("Received payload of " + data.Length + " characters \nRecieved:" + data);
                    showLocation();
                    
                });
            }
            catch (Exception e)
            {
                var we = e.InnerException as WebException;
                if (we != null)
                {
                    var resp = we.Response as HttpWebResponse;
                    var code = resp.StatusCode;
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show("RespCallback Exception raised! Message:" + we.Message + "HTTP Status: " + we.Status);
                    });
                }
                else
                    throw;
            }
        }
       
        
        protected void showLocation()
        {
            MapLayer m_PushpinLayer = new MapLayer();
            map.Children.Add(m_PushpinLayer);
            String markLocation = getdata;
            String loginStatus="";
            MessageBox.Show("this is marklocation sent -> " +markLocation);
            //Parsing to get Visibility
            if (markLocation.StartsWith("0")) MessageBox.Show("You stalker");
            else if (markLocation.StartsWith("1")) MessageBox.Show("Happy stalking!");
            markLocation = markLocation.Substring(1);
            //Parsing to get LoginFlag
            if (markLocation.StartsWith("0")) loginStatus="Offline";
            else if (markLocation.StartsWith("1")) loginStatus="Online";
            markLocation = markLocation.Substring(1);

            if (!markLocation.Equals("null"))
            {

                int j = markLocation.IndexOf(";");
                String locationReceived = markLocation.Substring(0, j);
                String timeReceived = markLocation.Substring(j + 1);
                MessageBox.Show("Here, location = " + locationReceived + "and time = " + timeReceived);

                if (!locationReceived.Equals("null"))
                {
                    i = markLocation.IndexOf(",");
                    Double latitude = Convert.ToDouble(locationReceived.Substring(0, i));
                    Double longitude = Convert.ToDouble(locationReceived.Substring(i + 1));
                    MessageBox.Show("here latitude = " + latitude + "and longitude=" + longitude);
                    LocationTimeBlock.Text = "Last seen : "+timeReceived+"    "+loginStatus;

                    GeoCoordinate coordinates = new GeoCoordinate(latitude, longitude);
                    map.SetView(coordinates, 10);


                    Pushpin pushpin1 = new Pushpin();
                    pushpin1.Background = new SolidColorBrush(Colors.Red);
                    pushpin1.Location = coordinates;
                    m_PushpinLayer.AddChild(pushpin1, pushpin1.Location);
                  
                }

                

                //getPresentLocation(); Dont delete! Can use the same stuff for real time tracking
            
            }
            

        }
    }

    
}