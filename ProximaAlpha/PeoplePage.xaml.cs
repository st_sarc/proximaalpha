﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using System.Device.Location;
using System.Threading;
namespace ProximaAlpha
{
    
    public partial class PeoplePage : PhoneApplicationPage
    {
       public String serializedUserInfo,signedInEmail,password,signedInUID,signedInName,visibilityOffLocation,action,halla,hallaUID;
        UserInfo current;
        int count,acceptFlag;
        int selectedIndex;
        public PeoplePage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("serializedUserInfo", out serializedUserInfo) && NavigationContext.QueryString.TryGetValue("signedInEmail", out signedInEmail) && NavigationContext.QueryString.TryGetValue("password", out password))
            {
                 current=(UserInfo) JsonConvert.DeserializeObject<UserInfo>(serializedUserInfo);
                 signedInUID = current.fromUID;
                 signedInName = current.fromName;
                 MessageBox.Show("Signedin uid:" + signedInUID);
                 ListBoxContacts.ItemsSource = current.contact;
                 acceptFlag = 0;
                 displayPendingRequests(current);
            }
        }

        protected void displayPendingRequests(UserInfo current)
        {
            if (!(current.requests[0].requestUID == ""))
            {
                count = 0;
                displayNextRequest(current);          
            }
        }

        private void displayNextRequest(UserInfo current) 
        { 
            String requestMessage = current.requests[count].requestMessage;
            String fromEmail = current.requests[count].fromEmail;
            String fromName = current.requests[count].fromName;
            String fromUID = current.requests[count].requestUID;

            if (requestMessage == "")
            {
                requestMessage = "Hi, I want to add you to my contacts!";
            }
            else
            {
                requestMessage = "\"" + requestMessage + "\"";
            }

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = "New Request",
                Message = fromName + " ( " + fromEmail + " )" + " says :\n" + requestMessage + "\n Do you want to accept?",
                LeftButtonContent = "yes",
                RightButtonContent = "no",
            };

            messageBox.Dismissed += (s1, e1) =>
            {
                switch (e1.Result)
                {
                     case CustomMessageBoxResult.LeftButton:
                        action = "acceptRequest";
                        acceptFlag = 1;
                        var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action=" + action + "&signedInUID=" + signedInUID + "&signedInName=" + signedInName + "&acceptedUID=" + fromUID + "&acceptedName=" + fromName) as HttpWebRequest;
                        request.Accept = "application/json;odata=verbose";
                        request.BeginGetResponse(new AsyncCallback(GotResponse), request);
                        break;
                    case CustomMessageBoxResult.RightButton:
                        action = "rejectRequest";
                        request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action=" + action + "&signedInUID=" + signedInUID + "&signedInName=" + signedInName + "&rejectedUID=" + fromUID + "&rejectedName=" + fromName) as HttpWebRequest;
                        request.Accept = "application/json;odata=verbose";
                        request.BeginGetResponse(new AsyncCallback(GotResponse), request);
                        break;
                    case CustomMessageBoxResult.None:
                        // Do something.
                        break;
                    
                    default:
                        break;
                }

                this.Dispatcher.BeginInvoke(() =>
                {
                    if (count < current.requests.Count - 1)
                    {
                        count++;
                        displayNextRequest(current);
                    }

                    else if (count == current.requests.Count-1&&acceptFlag==1) {
                        acceptFlag = 0;
                        action = "signIn";
                        var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action=" + action + "&signedInEmail=" + signedInEmail + "&password="+password) as HttpWebRequest;
                        request.Accept = "application/json;odata=verbose";
                        request.BeginGetResponse(new AsyncCallback(GotResponse), request);
                    }
                });
                        
            };
            messageBox.Show();
        } 

        private void GotResponse(IAsyncResult asynchronousResult)
        {
            try
            {
                string data;
                // State of request is asynchronous 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)asynchronousResult.AsyncState; ;
                using (HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.EndGetResponse(asynchronousResult))
                {
                    // Read the response into a Stream object. 
                    System.IO.Stream responseStream = response.GetResponseStream();
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        data = reader.ReadToEnd();
                       
                    }
                    responseStream.Close();
                }
                // Callback occurs on a background thread, so use Dispatcher to marshal back to the UI thread 
                this.Dispatcher.BeginInvoke(() =>
                {
                    if (data != "")
                    {
                        MessageBox.Show("Got JSON again "+data);
                        current = (UserInfo)JsonConvert.DeserializeObject<UserInfo>(data);
                        ListBoxContacts.ItemsSource = current.contact;
                    
                    }
                    
                  
                });
            }
            catch (Exception e)
            {
                var we = e.InnerException as WebException;
                if (we != null)
                {
                    var resp = we.Response as HttpWebResponse;
                    var code = resp.StatusCode;
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show("RespCallback Exception raised! Message:" + we.Message + "HTTP Status: " + we.Status);
                    });
                }
                else
                    throw;
            }
        }

        private void AddPeopleButton_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/AddContactPage.xaml?&signedInUID="+signedInUID, UriKind.Relative));
        }

        private void ListBoxContacts_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            selectedIndex = ListBoxContacts.SelectedIndex;
        // If selected index is -1 (no selection) do nothing
            if (ListBoxContacts.SelectedIndex == -1)
            {
                MessageBox.Show("This is being sent as selected index - > " + ListBoxContacts.SelectedIndex);
                return;
            }
        // Navigate to the new page
            NavigationService.Navigate(new Uri("/LocationPage.xaml?selectedIndex="+ListBoxContacts.SelectedIndex+"&serializedUserInfo="+serializedUserInfo, UriKind.Relative));
            
        // Reset selected index to -1 (no selection)
            ListBoxContacts.SelectedIndex = -1;
        }

        private void StackPanel_Hold_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            
            
           //MessageBox.Show(" abc "+visibilityOffLocation);
                             
            Contact c = (sender as StackPanel).DataContext as Contact;
            String toUID = c.uid;

            /*  halla = c.name;
            hallauid = c.uid;*/

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = "Toggle visibility",
                Message = "Set your visibility for "+halla,
                LeftButtonContent = "On",
                RightButtonContent = "Off",
            };
            
            messageBox.Dismissed += (s1, e1) =>
            {
                switch (e1.Result)
                {
                    case CustomMessageBoxResult.LeftButton:
                        action = "visibilityOn";
                      /*  int i=0;
                        while(c.uid!=current.contact[i].uid)i++;
                        MessageBox.Show(current.contact[i].visibility+" i="+i);
                        current.contact[i].visibility = "1";
                        ListBoxContacts.ItemsSource = current.contact; */
                        var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action=" + action + "&signedInUID=" + signedInUID + "&toUID=" + c.uid) as HttpWebRequest;
                        request.Accept = "application/json;odata=verbose";
                        request.BeginGetResponse(new AsyncCallback(GotResponse), request);
                        break;
                    case CustomMessageBoxResult.RightButton:
                        action = "visibilityOff";
                        /*  i=0;
                          while(c.uid!=current.contact[i].uid)i++;
                          MessageBox.Show(current.contact[i].visibility+" i="+i);
                          current.contact[i].visibility = "0";
                          ListBoxContacts.ItemsSource = current.contact;*/

                        GPSClass locator = new GPSClass(action,signedInUID,toUID);
                        locator.getGPScoordinates();
                        locator.flag = true;
                        break;

                    case CustomMessageBoxResult.None:
                        // Do something.
                        break;

                    default:
                        break;
                }

            };
            messageBox.Show();
            MessageBox.Show("HELLO This is the selected index -> "+selectedIndex);
        }
       
    
    }

    public class Request
    {
        public string requestUID { get; set; }
        public string fromName { get; set; }
        public string fromEmail { get; set; }
        public string requestMessage { get; set; }
    }

        
    public class Contact
    {
            public string uid { get; set; }
            public string name { get; set; }
            public string visibility { get; set; }
            public string lastLocated { get; set; }
            public string locatedTime { get; set; }
    }

        
    public class UserInfo
    {
            public string fromUID { get; set; }
            public string fromName { get; set; }
            public List<Request> requests { get; set; }
            public List<Contact> contact { get; set; }
    }



    public class GPSClass {

        public GeoCoordinateWatcher gps;
        public Double latitude,longitude;
        public String action, signedInUID, toUID, visibilityOffLocation;
        public Boolean flag;

        public GPSClass(String x, String y, String z) {
            action = x;
            signedInUID = y;
            toUID = z;
        
        }

        public void getGPScoordinates()
        {
            gps = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
            gps.MovementThreshold = 20;
            gps.PositionChanged += gps_PositionChanged;
            gps.Start();
        }

        void gps_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            MessageBox.Show("GPS :Latitude :" + e.Position.Location.Latitude.ToString("0.0000") + "\nLongitude :" + e.Position.Location.Longitude.ToString("0.0000"));
            latitude = e.Position.Location.Latitude;
            longitude = e.Position.Location.Longitude;
            visibilityOffLocation=latitude+","+longitude;
            if(flag)makeVisibilityOffRequest();
        }

        public void gpsStop()
        {
            if (gps != null)
                gps.Stop();
        }

        public void makeVisibilityOffRequest(){
            gpsStop();
            flag = false;
            MessageBox.Show("Making request: "+visibilityOffLocation);
            var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action=" + action + "&signedInUID=" + signedInUID + "&toUID=" + toUID + "&visibilityOffLocation=" + visibilityOffLocation) as HttpWebRequest;
            request.Accept = "application/json;odata=verbose";
            request.BeginGetResponse(new AsyncCallback(GotResponse), request);
        }

        private void GotResponse(IAsyncResult asynchronousResult)
        {
            try
            {
                string data;
                // State of request is asynchronous 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)asynchronousResult.AsyncState; ;
                using (HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.EndGetResponse(asynchronousResult))
                {
                    // Read the response into a Stream object. 
                    System.IO.Stream responseStream = response.GetResponseStream();
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        data = reader.ReadToEnd();

                    }
                    responseStream.Close();
                }
                // Callback occurs on a background thread, so use Dispatcher to marshal back to the UI thread 
              
            }
            catch (Exception e)
            {
                var we = e.InnerException as WebException;
                if (we != null)
                {
                    var resp = we.Response as HttpWebResponse;
                    var code = resp.StatusCode;
                 /*   this.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show("RespCallback Exception raised! Message:" + we.Message + "HTTP Status: " + we.Status);
                    });*/
                }
                else
                    throw;
            }
        }
    
    }


    public class LocationSender {
        String location;

        public LocationSender() {
            ThreadPool.QueueUserWorkItem(sendLocation);
        }

        public void sendLocation(object state)
        {
                String action = "test";
                location = "23.4,67.90";
                var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action=" +action) as HttpWebRequest;
                request.Accept = "application/json;odata=verbose";
              //  request.BeginGetResponse(new AsyncCallback(GotResponse), request);
           
        }

        private void GotResponse(IAsyncResult asynchronousResult)
        {
            try
            {
                string data;
                // State of request is asynchronous 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)asynchronousResult.AsyncState; ;
                using (HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.EndGetResponse(asynchronousResult))
                {
                    // Read the response into a Stream object. 
                    System.IO.Stream responseStream = response.GetResponseStream();
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        data = reader.ReadToEnd();

                    }
                    responseStream.Close();
                }
                // Callback occurs on a background thread, so use Dispatcher to marshal back to the UI thread 

            }
            catch (Exception e)
            {
                var we = e.InnerException as WebException;
                if (we != null)
                {
                    var resp = we.Response as HttpWebResponse;
                    var code = resp.StatusCode;
                    /*   this.Dispatcher.BeginInvoke(() =>
                       {
                           MessageBox.Show("RespCallback Exception raised! Message:" + we.Message + "HTTP Status: " + we.Status);
                       });*/
                }
                else
                    throw;
            }
        }

       
    
    }


    }