﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Text.RegularExpressions;


namespace ProximaAlpha
{
    public partial class RegisterPage : PhoneApplicationPage
    {
        String action = "SignUp";
        public RegisterPage()
        {
            InitializeComponent();
        }

        private void SignUpButton_Click(object sender, RoutedEventArgs e)
        {
            if (NameBoxReg.Text == "" || EmailBoxReg.Text == "" || PasswordBoxReg.Password == "" || RepeatPasswordBoxReg.Password == "") MessageBox.Show("Please fill in all the fields");

            else
            {
                /*do sign up request*/
                var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action="+action+ "&name=" + NameBoxReg.Text + "&signedUpEmail=" +EmailBoxReg.Text + "&password=" + PasswordBoxReg.Password) as HttpWebRequest;
                request.Accept = "application/json;odata=verbose";
                request.BeginGetResponse(new AsyncCallback(GotResponse), request);
                
            }
        }

        private void EmailBoxReg_LostFocus_1(object sender, RoutedEventArgs e)
        {
            Regex emailregex = new Regex("(?<user>[^@]+)@(?<host>[^.@]+).([^@]+)");
            Match m = emailregex.Match(EmailBoxReg.Text);
            if (!m.Success)
            {
                if (EmailBoxReg.Text != "")
                {
                    MessageBox.Show("Please enter a valid email Address");
                    EmailBoxReg.Text = "";
                }
            }

        }

        private void PasswordBoxReg_LostFocus_1(object sender, RoutedEventArgs e)
        {
            if (PasswordBoxReg.Password.Length < 6)
            {
                MessageBox.Show("Your password must be at least 6 characters long");
                PasswordBoxReg.Password = "";
            }

        }


        private void RepeatPasswordBoxReg_LostFocus_1(object sender, RoutedEventArgs e)
        {
            if (RepeatPasswordBoxReg.Password != PasswordBoxReg.Password)
            {
                MessageBox.Show("The passwords don't match. Please try again");
                RepeatPasswordBoxReg.Password = "";
            }
        }

        private void PasswordBoxReg_PasswordChanged_1(object sender, RoutedEventArgs e)
        {
            RepeatPasswordBoxReg.Password = "";
        }

        private void GotResponse(IAsyncResult asynchronousResult)
        {
            try
            {
                string data;
                // State of request is asynchronous 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)asynchronousResult.AsyncState; ;
                using (HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.EndGetResponse(asynchronousResult))
                {
                    // Read the response into a Stream object. 
                    System.IO.Stream responseStream = response.GetResponseStream();
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        data = reader.ReadToEnd();
                    }
                    responseStream.Close();
                }
                // Callback occurs on a background thread, so use Dispatcher to marshal back to the UI thread 
                this.Dispatcher.BeginInvoke(() =>
                {
                    if (data == "1000")
                        MessageBox.Show("User details not entered in database:" + data);
                    else if (data == "2000")
                    {
                        MessageBox.Show("This Email ID already exists.");
                        MessageBox.Show(data);
                        EmailBoxReg.Text = "";
                    }
                    else
                    {
                        //navigate
                        MessageBox.Show(data);
                        NavigationService.Navigate(new Uri("/SignUpSuccessPage.xaml", UriKind.Relative));
                    }
                });
            }
            catch (Exception e)
            {
                var we = e.InnerException as WebException;
                if (we != null)
                {
                    var resp = we.Response as HttpWebResponse;
                    var code = resp.StatusCode;
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show("RespCallback Exception raised! Message:" + we.Message + "HTTP Status: " + we.Status);
                    });
                }
                else
                    throw;
            }
        }
    }
}