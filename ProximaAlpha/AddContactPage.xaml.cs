﻿using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace ProximaAlpha
{

    public partial class Page1 : PhoneApplicationPage
    {
        String EMAILDOESNOTEXIST = "1000", WRONGPASSWORD = "2000", NOCONTACTS = "3000", ADDREQUESTSUCCESS = "4000", ADDREQUESTFAILURE = "5000", EMAILALREADYEXISTS = "6000", REQUESTALREADYEXISTS = "7000";
        String action = "AddNewContact";
        String getData;
        String signedInUID;
        
        public Page1()
        {
            InitializeComponent();
        }



        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("signedInUID", out signedInUID))
            {
                MessageBox.Show("UID passed "+signedInUID);
            }
            

        }



        private void AddButton_Click_1(object sender, RoutedEventArgs e)
        {
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Message = "Are you sure you want to add "+EmailIDBoxAdd.Text+" to your contacts?",
                LeftButtonContent = "yes",
                RightButtonContent = "no"
            };

            messageBox.Dismissed += (s1, e1) =>
            {
                switch (e1.Result)
                {
                    case CustomMessageBoxResult.LeftButton:
                        var request = HttpWebRequest.Create("http://localhost:8084/ProximaAlpha/ProximaAlphaServlet?action="+action+"&signedInUID="+signedInUID+ "&toEmail=" +EmailIDBoxAdd.Text+"&requestMessage="+MessageBoxAdd.Text) as HttpWebRequest;
                        request.Accept = "application/json;odata=verbose";
                        request.BeginGetResponse(new AsyncCallback(GotResponse), request);
                        break;
                    case CustomMessageBoxResult.RightButton:
                        MessageBox.Show("You pressed No");    
                    // Do something.
                        break;
                    case CustomMessageBoxResult.None:
                        // Do something.
                        break;
                    default:
                        break;
                }
            };

            messageBox.Show(); 

        }

        private void GotResponse(IAsyncResult asynchronousResult)
        {
            try
            {
                string data;
                // State of request is asynchronous 
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)asynchronousResult.AsyncState; ;
                using (HttpWebResponse response = (HttpWebResponse)myHttpWebRequest.EndGetResponse(asynchronousResult))
                {
                    // Read the response into a Stream object. 
                    System.IO.Stream responseStream = response.GetResponseStream();
                    using (var reader = new System.IO.StreamReader(responseStream))
                    {
                        data = reader.ReadToEnd();
                    }
                    responseStream.Close();
                }
                // Callback occurs on a background thread, so use Dispatcher to marshal back to the UI thread 
                this.Dispatcher.BeginInvoke(() =>
                {
                    getData = data;
                    printResult();

                });
            }
            catch (Exception e)
            {
                var we = e.InnerException as WebException;
                if (we != null)
                {
                    var resp = we.Response as HttpWebResponse;
                    var code = resp.StatusCode;
                    this.Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show("RespCallback Exception raised! Message:" + we.Message + "HTTP Status: " + we.Status);
                    });
                }
                else
                    throw;
            }
        }

        protected void printResult() {

            if (getData==EMAILDOESNOTEXIST)
            {
                MessageBox.Show("This Email ID may not have signed up");
            }
            if (getData == ADDREQUESTFAILURE)
            {
                MessageBox.Show("Add request failed.");
            }

            else if (getData == ADDREQUESTSUCCESS)
            {
                MessageBox.Show("Request sent successfully");
            }

            else if (getData == EMAILALREADYEXISTS) 
            {
                MessageBox.Show("This person already exists in your contacts!");
            }
            else if (getData == REQUESTALREADYEXISTS)
            {
                MessageBox.Show("You have already sent a request!");
            }

            else MessageBox.Show(getData);
        }
    }
}